<?php
require __DIR__ . '/vendor/autoload.php';

header('Content-Type: application/xml');

if(!isset($_POST['file_url']) || empty($_POST['file_url'])) {
	$file = 'https://www.serveris.lv/cms/test.json';
} else {
	/* Tested:
	  *	https://jsonplaceholder.typicode.com/todos/1
	  * https://jsonplaceholder.typicode.com/comments
	*/
	$file = $_POST['file_url'];
}

$jsonData = file_get_contents($file);
$rootNode['childElement'] = json_decode($jsonData);
$converter = new \SalernoLabs\PHPToXML\Convert();
$xml = $converter
	->setObjectData($rootNode)
	->convert();

echo $xml;

?>
