<?php
$pages = Array(
	Array('title' => 'Tehnika', 'id' => 1, 'parent' => 0),
	Array('title' => 'Mēbeles', 'id' => 2, 'parent' => 0),
	Array('title' => 'Telefoni', 'id' => 3, 'parent' => 1),
	Array('title' => 'Sadzīves tehnika', 'id' => 4, 'parent' => 1),
	Array('title' => 'Nokia', 'id' => 5, 'parent' => 3),
	Array('title' => 'Samsung', 'id' => 6, 'parent' => 3),
	Array('title' => 'Veļas mašīnas', 'id' => 7, 'parent' => 4),
	Array('title' => 'Putekļu sūcēji', 'id' => 8, 'parent' => 4),
	Array('title' => 'Whirlpool', 'id' => 11, 'parent' => 7),
	Array('title' => 'Electrolux', 'id' => 12, 'parent' => 7),
	Array('title' => 'Roboti', 'id' => 9, 'parent' => 8),
	Array('title' => 'Slapjā sūkšana', 'id' => 10, 'parent' => 8),
);

$page_level = array();

function getFirstLevel() {
	global $pages;
	$first_menu_level = '';
	
	foreach($pages as $page) {
		if($page['parent'] == 0) {
			$first_menu_level = $first_menu_level.$page['title'].'<br>';
		}
	}
	
	return $first_menu_level;
}

function getMenu($id) {
	global $pages;
	global $page_level;

	$r = array();
	$i = 0;
	$menu = '';

	foreach($pages as $page) {
		if ($page['parent'] == $id) {
			if($page['parent'] == 0) {
				$page_level[$page['id']]['i'] = 0;
			} else {
				$page_level[$page['id']]['i'] = $page_level[$page['parent']]['i'] + 1;
			}
			
			$menu = $menu.
				'<div style="margin:0px '.($page_level[$page['id']]['i'] * 35).'px;">'.
					$page['title'].
				'</div>';
			$menu = $menu.getMenu($page['id']);
			$i++;
		}
	}

	return $menu;
}
?>