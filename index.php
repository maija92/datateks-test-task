<?php
	include 'pages.php';
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title></title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<div class="jumbotron text-center">
				<h1>Datateks Test Task</h1>
				<p>Maija Černovska</p>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<h3>Task 1.1</h3>
						<?=getFirstLevel();?>
					</div>
					<div class="col-sm-4">
						<h3>Task 1.2</h3>
						<?=getMenu(0);?>
					</div>
					<div class="col-sm-4">
						<h3>Task 2</h3>
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" href="#url" role="tab" data-toggle="tab" aria-selected="true">URL</a>
							</li>
						</ul>
						<div class="tab-content pt-3">
							<div role="tabpanel" class="tab-pane fade in active show" id="url">
							<form target="_blank" action="json.php" method="post">
								<div class="input-group mb-3">
									<input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1" name="file_url">
									<div class="input-group-append">
										<button class="btn btn-outline-secondary" type="submit" id="button-addon1">Get XML</button>
									</div>
								</div>
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	</body>
</html>
